include('shared.lua')

	function ENT:Draw()
		self:DrawModel()
	end

	function ENT:Think()
		self:NextThink(CurTime())

		self.PP_Int = self.PP_Int or 0
		local vel = self:GetVelocity():Length()
		local kmh = vel * 0.09144
		local dir = self:LocalToWorld(self:OBBCenter()) + self:GetVelocity()
		local forward = (dir - self:WorldSpaceCenter()):Cross(
		dir - self:GetPos()):Dot(self:GetForward())

		if vel < 5 or not self:Is_OnGround() then return true end

		self.PP_Int = Lerp(.5, self.PP_Int, self.PP_Int + (kmh * (forward > 0 and 1 or -1)))
		self:SetPoseParameter("vehicle_wheel_spin", self.PP_Int)

		return true
	end