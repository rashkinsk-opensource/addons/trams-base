AddCSLuaFile()

ENT.Type	= "anim"
ENT.PrintName	= "TramBase"
ENT.Category	= "Raskinsk tram's wheels"
ENT.Model = "models/dk_cars/rashkinsk/tatra/wh.mdl"
ENT.Spawnable = false
ENT.AdminOnly = false

ENT.ConnectVec = Vector(0,2.7,30.9)
ENT.ConnectBone = "ax"
ENT.CustomSlider = nil -- "models/sligwolf/unique_props/sw_slider.mdl"  - default
ENT.Sliders = {
	{Vector(0,42.9), Angle(0,90)},
	{Vector(0,-36.7), Angle(0,90)},
}

function ENT:Is_OnGround()
	local tr = util.TraceLine( {
		start = self:GetPos() + self:GetAngles():Up() * 10,
		endpos = self:GetPos(),
		filter = self
	} )

	return tr.Hit
end