AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

	function ENT:Initialize()
		self:SetModel( self.Model )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_STEP )
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetUseType( SIMPLE_USE )
		self:SetNoDraw(false)
		local phys = self:GetPhysicsObject()
        phys:Wake()
		phys:SetMass(10000)
		phys:EnableMotion( true )
	end

	function ENT:SpawnFunction( ply, tr, ClassName )

		if ( !tr.Hit ) then return end

		local SpawnPos = tr.HitPos + tr.HitNormal * 1

		local ent = ents.Create( ClassName )
		ent:SetPos( SpawnPos )
		ent:Spawn()

		return ent

	end