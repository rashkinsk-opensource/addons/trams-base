AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')
	function ENT:Initialize()
		self:SetModel( self.Model )
		self:SetSolid( SOLID_BBOX )
		self:SetMoveType( MOVETYPE_STEP )
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetUseType( SIMPLE_USE )
		self:SetNoDraw(false)
		local phys = self:GetPhysicsObject()
		if not IsValid( phys ) then return end
        phys:Sleep()
        phys:SetMaterial("metalvehicle")
		phys:SetMass(self.Mass or 1000)

		phys:EnableMotion( true )
		self.DriverSeat = ents.Create("prop_vehicle_prisoner_pod")
		self.DriverSeat:SetMoveType( MOVETYPE_NONE )
		self.DriverSeat:SetModel( "models/nova/airboat_seat.mdl" )
		self.DriverSeat:SetKeyValue( "vehiclescript","scripts/vehicles/prisoner_pod.txt" )
		self.DriverSeat:SetKeyValue( "limitview", 0 )
		self.DriverSeat:SetPos( self:LocalToWorld(self.SeatPos) )
		self.DriverSeat:SetAngles( self:LocalToWorldAngles(self.SeatAng) )
		self.DriverSeat:SetParent(self)
		self.DriverSeat:SetOwner( self )
		self.DriverSeat:Spawn()
		self.DriverSeat:Activate()
		self.DriverSeat:SetNotSolid( true )
		self.DriverSeat:SetNoDraw( true )
		self.DriverSeat:DrawShadow( false )
		self.DriverSeat:GetPhysicsObject():EnableDrag( false ) 
		self.DriverSeat:GetPhysicsObject():EnableMotion( false )
		self.DriverSeat:GetPhysicsObject():SetMass( 1 )
		self.DriverSeat:SetCollisionGroup( COLLISION_GROUP_NONE )
		self.DriverSeat.Tram = self
		self.DriverSeat:SetNWInt("SeatType", 0)

		self.PassengerSeats = {}

		for k, v in pairs(self.PassengerSeatsD) do
			seat = ents.Create("prop_vehicle_prisoner_pod")
			seat:SetMoveType( MOVETYPE_NONE )
			seat:SetModel( "models/nova/airboat_seat.mdl" )
			seat:SetKeyValue( "vehiclescript","scripts/vehicles/prisoner_pod.txt" )
			seat:SetKeyValue( "limitview", 0 )
			seat:SetPos( self:LocalToWorld(v[1]) )
			seat:SetAngles( self:LocalToWorldAngles(v[2]) )
			seat:SetParent(self)
			seat:SetOwner( self )
			seat:Spawn()
			seat:Activate()
			seat:SetNotSolid( true )
			seat:SetNoDraw( true )
			seat:DrawShadow( true )
			seat:GetPhysicsObject():EnableDrag( false ) 
			seat:GetPhysicsObject():EnableMotion( false )
			seat:GetPhysicsObject():SetMass( 1 )
			seat:SetCollisionGroup( COLLISION_GROUP_NONE )
			seat.Tram = self
			seat:SetNWInt("SeatType", 1)

			table.insert(self.PassengerSeats, seat)
		end

		local d = ents.Create("prop_physics")
		if IsValid(d) then
			d:SetPos(self:LocalToWorld(self.DoorLock[1]))
			d:SetAngles(self:LocalToWorldAngles(self.DoorLock[2]))
			d:SetModel(self.DoorLock[3])
			d:Spawn()
			d:SetParent(self)
			d:SetOwner(self)
			d:SetNoDraw(true)
			self.Dor = d
			d.Own = self
			self:CallOnRemove("RemovDor",function() if IsValid(self.Dor) then self.Dor:Remove() end end)
			self.Dor:CallOnRemove("dorRemovDor",function() if IsValid(self.Dor.Own) then self.Dor.Own:Remove() end end)
		end

		self.Wheels = {}
		for k, v in pairs(self.WheelPos) do
			local ent = ents.Create("rshk_tram_wheels")
			ent:SetModel(self.Slider.Model)
			ent.Sliders = self.Slider.Sliders
			ent.ConnectVec = self.Slider.ConnectVec

			ent:SetPos(self:LocalToWorld(v[1]))
			ent:SetAngles(self:LocalToWorldAngles(v[2]))
			ent:Spawn()
			ent:Activate()
			constraint.Axis( ent, self, 0, 0, ent.ConnectVec, v[3], 0, 0, 1, 1, nil, false )
			
			table.insert(self.Wheels, ent)
		end
		self:CallOnRemove("RemoveWheels",function() for k, v in pairs(self.Wheels) do if IsValid(v) then v:Remove() end end end)
		
		self:SetNWEntity("TramDriverSeat", self.DriverSeat)
	end

	local function NearestSeat(ply, ent)
		local pos = ply:GetPos()
		local r = math.huge
		local mindistance, seat = r^2
		for i, v in ipairs(ent.PassengerSeats) do
			local distance = v:GetPos():DistToSqr(pos)
			if distance < mindistance then
				mindistance, seat = distance, v
			end
		end
		
		return seat
	end

	function ENT:Use(ply)
		if self.DriverSeat and not IsValid(self.DriverSeat:GetDriver()) and not ply:KeyDown(IN_WALK) then
			if ply == self.Player then
				 ply:EnterVehicle(self.DriverSeat) 
			else
				ply:ChatPrint("Вы не можете сесть в трамвай, так как он не ваш!")
			end
		else
			local seat = NearestSeat(ply, self)
			if IsValid(seat) then ply:EnterVehicle(seat) end
		end
	end

	function ENT:DoBrake(throttle, snd)
		local dir = self:LocalToWorld(self:OBBCenter()) + self:GetVelocity()
		local forward = (dir - self:WorldSpaceCenter()):Cross(
		dir - self:GetPos()):Dot(self:GetForward())
		self.Force = self.Wheels[1]:GetAngles():Right()*-(throttle*(forward > 0 and -1 or 1))

		if self:GetVelocity():Length() > 100 and not snd then
			self.BSound:Play()
			self.BSound:ChangePitch(100, 0)
			self.BSound:ChangeVolume(1, 0)
		else
			self.BSound:ChangePitch(0, 0)
			self.BSound:ChangeVolume(0, 0)
			self.BSound:Stop()
		end
	end


	local function NearestVector(vec, vectors)
		local pos = vec
		local r = math.huge
		local mindistance, seat = r^2
		for i, v in pairs(vectors) do
			local distance = v[4]:DistToSqr(pos)
			if distance < mindistance then
				mindistance, seat = distance, v
			end
		end
		
		return seat
	end

	local idle = "rshk_tram/tram_cruise.wav"
	local bsound = "rshk_tram/flange_10.wav"
	local st = file.Exists("rshk_trams/"..game.GetMap().."/stantions.dat","DATA") and util.JSONToTable(file.Read("rshk_trams/"..game.GetMap().."/stantions.dat", "DATA")) or nil
	function ENT:Think()
		self:NextThink(CurTime())
		if not self.Sound then
			self.Sound = CreateSound(self, idle)
			self.Sound:PlayEx(0, 0)
			self:CallOnRemove("StopSound",function() self.Sound:Stop() end)
		end
		if not self.BSound then
			self.BSound = CreateSound(self, bsound)
			self.BSound:PlayEx(0, 0)
			self:CallOnRemove("StopBSound",function() self.BSound:Stop() end)
		end

		if st then
			self.Played = self.Played or {}
			local pp = self:GetPoseParameter("vehicle_doors")
			for k, v in pairs(st) do
				local vec = (NearestVector(self:GetPos(), st))
				if vec and v[4]:Distance(self:GetPos()) <= 500 then
					self.Stant = k
					if self.Played[2] != v[5] and !self.Doors and self.Played[1] != nil then
						self.StantS = CreateSound(self, v[5])
						self.StantS:PlayEx(1,100)
						self.Played[2] = v[5]
						timer.Simple(v[6], function()
							self.StantS:Stop()
						end)
					elseif self.Played[1] != v[3] and self.Doors then
						if self.StantS then self.StantS:Stop() end
						self:EmitSound(v[3])
						self.Played[1] = v[3]
						self:SetNWBool("AtStant", true)
					end
					break
				else if self.Stant and self.Stant == k then self.Played = {}; self:SetNWBool("AtStant", false); if self.StantS then self.StantS:Stop() end end end
			end
		end

		self.ToThrottle = self.ToThrottle or 0
		self.Gear = self.Gear or 1
		self.Throttle = self.Throttle and self:WaterLevel() < 2 and Lerp(.5, self.Throttle, self.ToThrottle) or 0
		self.DoorsInt = self.DoorsInt or 0
		self.YsikiInt = self.YsikiInt or 0
		self.Ysiki = self.Ysiki or false
		local phys = self.Wheels[1]:GetPhysicsObject()

		self.Driver = self.DriverSeat:GetDriver()

		if self.LastDriver and self.LastDriver != self.Driver then
			if IsValid(self.LastDriver) then self.LastDriver:SetNWBool("DrivingTram", false) end
			self.LastDriver = self.Driver
			self.Driver:SetNWBool("DrivingTram", true)
		else
			self.LastDriver = self.Driver
		end

		self:SetNWEntity("TramDriver", self.Driver)
		self:SetNWInt("TramPower", self.Throttle)
		self:SetNWInt("TramGear", self.Gear)
		self.Force = self.Force or Vector()
		local speed = (self:GetVelocity():Length() * 0.09144)
		if self.Throttle <= 1000 and speed > 0 or self:WaterLevel() > 1 then -- торможение
			self:DoBrake(self.MaxThrottle+(self:GetVelocity():Length()*10))
		else 
			if self.BSound then self.BSound:ChangePitch(100, 0); self.BSound:ChangeVolume(1, 0); self.BSound:Stop() end
			self.Force = self.Wheels[1]:GetAngles():Right()*-(self.Throttle*self.Gear)
		end
		if speed > 0 then -- тут, короче, пытаемся сделать звуки
			local Speed =  self:GetVelocity():Length()
			local UnitKmh = math.Round((Speed * 0.75) * 3600 * 0.0000254)
			local pitch = math.Clamp(UnitKmh * 1.9, 0, 255)

			self.Sound:Play()
			self.Sound:ChangePitch(pitch, 0)
			self.Sound:ChangeVolume(1, 0)
		else
			self.Sound:ChangePitch(0, 0)
			self.Sound:ChangeVolume(0, 0)
			self.Sound:Stop()
		end

		if self.MaxSpeed and speed >= self.MaxSpeed then
			self:DoBrake(self.MaxThrottle/4, true)
		end

		self:SetNWBool("IsSND", self.StantS and self.StantS:IsPlaying() or false)
		if self:GetNWBool("TramDoors") then
			self.DoorsInt = Lerp(.05, self.DoorsInt,1)
			if IsValid(self.Dor) then
				self.Dor:SetCollisionGroup( COLLISION_GROUP_WORLD )
			end
		elseif (self.StantS and !self.StantS:IsPlaying() and !self.Doors) or !self:GetNWBool("AtStant", false) then
			self.DoorsInt = Lerp(.05, self.DoorsInt, 0)
			if IsValid(self.Dor) then
				self.Dor:SetCollisionGroup( COLLISION_GROUP_NONE )
			end
		end

		if self.Ysiki then
			self.YsikiInt = Lerp(.1, self.YsikiInt, 1)
		else
			self.YsikiInt = Lerp(.1, self.YsikiInt, 0)
		end
		self:SetPoseParameter("vehicle_doors",self.DoorsInt)
		self:SetPoseParameter("vehicle_ysiki",self.YsikiInt)

		phys:ApplyForceOffset(self.Force, self:LocalToWorld(self.WheelPos.Front[1]))
		if IsValid(self.Driver) then
			local ply = self.Driver
			self:SetNWInt("Transport_NextStation", ply:GetNWInt("Transport_NextStation"))

			local W, A, S, D, Space, L, R, Shift = ply:KeyDown(IN_FORWARD), ply:KeyDown(IN_MOVELEFT), ply:KeyDown(IN_BACK), ply:KeyDown(IN_MOVERIGHT), ply:KeyDown(IN_JUMP),
			ply:KeyDown(IN_ATTACK), ply:KeyDown(IN_ATTACK2), ply:KeyDown(IN_SPEED)

			if L then
				self.Gear = 1
			elseif R then self.Gear = -1 end

			if W then
				if self.ToThrottle < self.MaxThrottle then
					self.ToThrottle = self.MaxThrottle--self.ToThrottle+250
				else self.ToThrottle = self.MaxThrottle end
			elseif S then 
				if self.ToThrottle > 0 then
					if self.ToThrottle <= self.MaxThrottle then
						self.ToThrottle = 0--self.ToThrottle-250
					else self.ToThrottle = self.MaxThrottle end
				else self.ToThrottle = 0 end
			else self.ToThrottle = 1500 end
			if ply:KeyDown(IN_RELOAD) then
				if not self.Horn then
					self.Horn = CreateSound(self, "trams/misc/horn_1.wav")
					self.Horn:PlayEx(0, 0)
				else
					self.Horn:ChangePitch(100, 0)
					self.Horn:ChangeVolume(1, 0)
				end
			else
				if self.Horn then
					self.Horn:ChangePitch(0, 0)
					self.Horn:ChangeVolume(0, 0)
				end
	        end
	    
			if ply:KeyPressed(IN_MOVELEFT) and not self:GetNWBool("TramDoors") then self:EmitSound("vehicles/_tails_/ikarus/door_open.wav") end
			if ply:KeyPressed(IN_MOVERIGHT) and self:GetNWBool("TramDoors") then self:EmitSound("vehicles/_tails_/ikarus/door_open.wav") end
			if A then
				self:SetNWBool("TramDoors", true)
			elseif D then self:SetNWBool("TramDoors", false) end
		end

		return true
	end

	function ENT:SpawnFunction( ply, tr, ClassName )

		if ( !tr.Hit ) then return end

		local SpawnPos = tr.HitPos + tr.HitNormal * 50
		local SpawnAng = ply:EyeAngles()
		SpawnAng.p = 0
		SpawnAng.y = SpawnAng.y + 180

		local ent = ents.Create( ClassName )
		ent:SetPos( SpawnPos )
		ent:SetAngles( SpawnAng )
		ent:Spawn()
		ent:Activate()

		return ent

	end

	hook.Add("PlayerButtonDown", "Trams_Buttons", function(ply, key) -- Я решил не юзать numpad.Register т.к. он лагает
		local veh = ply:GetVehicle()
		if !veh.Tram then return end
		local ent = veh.Tram

		if key == KEY_F then
			ent:SetNWBool("Tram_RunningActive", !ent:GetNWBool("Tram_RunningActive", false))
		elseif key == 109 then
			ent:SetNWBool("Tram_HazardActive", !ent:GetNWBool("Tram_HazardActive", false))
		end
	end)

	hook.Add("PlayerLeaveVehicle", "NiceLeaveTram", function(ply, ent)
		if !ent.Tram then return end

		local tr = util.TraceHull( {
			start = ent:GetPos()-Vector(0,0,10),
			endpos = ent:GetPos()-Vector(0,0,10),
			mins = ply:OBBMins(),
			maxs = ply:OBBMaxs(),
			filter = {ply, ent},
		})


		if !tr.Hit then
			ply:SetPos(tr.HitPos)
		else
			for i=5, math.abs(ent.Tram:OBBMins().y)+math.abs(ent.Tram:OBBMaxs().y)-10 do
				local p = ent.Tram:LocalToWorld(Vector(0,ent.Tram:OBBMaxs().y-i,ent.Tram:WorldToLocal(ent.Tram.DriverSeat:GetPos()).z-22))

				local tr2 = util.TraceHull( {
					start = p,
					endpos = p,
					mins = ply:OBBMins(),
					maxs = ply:OBBMaxs(),
					filter = {ply, ent},
				})

				if not tr2.Hit then
					ply:SetPos(tr2.HitPos)
					break
				end
			end
		end
	end)

	util.AddNetworkString("TramSyncDat")
	--[[-------------------------------------------------------------------------
	cls - класс
	tbl - тейбл листа с датой(list.Get("rshk_trams")[класс])
	SpawnAng - Angle спавна
	ply - Игрок(Для того, чтобы добавить undo, можно оставить nil)
	tr - Trace глаз игрока(ply:GetEyeTrace()), но если надо спавнить на опред. позиции, то вместо аргумента "tr" (trace) делайте вектор позицию спавна
	---------------------------------------------------------------------------]]
	function SpawnTram(cls, SpawnAng, ply, tr)
		local tbl = list.Get("rshk_trams")[cls]
		local tram = ents.Create("rshk_tram_base")
		if not tram then print("UNABLE TO SPAWN TRAM") return end
		tram:SetPos(not isvector(tr) and (tr.HitPos + tr.HitNormal * (tbl.SpawnMult or 50)) or isvector(tr) and tr)
		tram:SetAngles(SpawnAng)
		tram:SetModel(tbl.Model)
		tram.MaxSpeed = tbl.MaxSpeed
		tram.MaxThrottle = tbl.MaxThrottle
		tram.SeatPos = tbl.SeatPos
		tram.SeatAng = tbl.SeatAng
		tram.WheelPos = tbl.WheelPos
		tram.Slider = tbl.Slider
		tram.PassengerSeatsD = tbl.PassengerSeatsD
		tram.Mass = tbl.Mass
		tram.DoorLock = tbl.DoorLock
		tram:Spawn()
		tram:Activate()
		if ply then
			tram.Player = ply
		end
		
		timer.Simple(.1, function()
    		net.Start("TramSyncDat")
    			net.WriteTable(tbl.LightTable)
    			net.WriteEntity(tram)
    		net.Broadcast()
        end)
    
		return tram
	end