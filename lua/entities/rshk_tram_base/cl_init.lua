include('shared.lua')

	function ENT:Draw()
		self:DrawModel()
	end

	function ENT:Think()
		self:NextThink(CurTime())
		self.DoorsInt = self.DoorsInt or 0
		if self:GetNWBool("TramDoors", false) then
			self.DoorsInt = Lerp(.025, self.DoorsInt,1)
		elseif (!self:GetNWBool("IsSND", false) and !self.Doors) or !self:GetNWBool("AtStant", false) then
			self.DoorsInt = Lerp(.025, self.DoorsInt, 0)
		end

		self:SetPoseParameter("vehicle_doors",self.DoorsInt)

		return true
	end

	hook.Add("CalcVehicleView", "CalcTramView", function(ent, ply, view)
		if IsValid(ent:GetParent()) then
			local veh = ent:GetParent()
			if veh.Tram and ent == veh:GetChildren()[1] then
				if ent:GetThirdPersonMode() then
					local tr = util.TraceLine( {
						start = Vector(veh:GetPos().x, veh:GetPos().y, ply:EyePos().z),
						endpos = Vector(veh:GetPos().x, veh:GetPos().y, ply:EyePos().z) - ply:GetAimVector() * 800 *(ent:GetCameraDistance() <= 0 and 0 or ent:GetCameraDistance() >= 2 and 2 or ent:GetCameraDistance()),
						filter = {ply, ent, veh}
					} )
					ply.ViewVec = ply.ViewVec and LerpVector(.5, ply.ViewVec, tr.HitPos) or tr.HitPos
					ply.ViewAng = ply.ViewAng and LerpAngle(.5, ply.ViewAng, view.angles) or view.angles
					local newview = {}
					newview.origin = ply.ViewVec
					newview.drawviewer = true
					newview.angles = ply.ViewAng

					return newview
				else
					ply.ViewVec = view.origin
					ply.ViewAng = view.angles
				end
			else
				ply.ViewVec = view.origin
				ply.ViewAng = view.angles
			end
		else
			ply.ViewVec = view.origin
			ply.ViewAng = view.angles
		end
	end)

	local function ShadowText(text, col, pos)
		draw.SimpleText(text,"DermaLarge",pos.x+2,pos.y+2,color_black,0,0)
		draw.SimpleText(text,"DermaLarge",pos.x,pos.y,col or color_white,0,0)
	end
	
	hook.Add("HUDPaint","PaintTramHUD",function()
		local ply = LocalPlayer()
		if not IsValid(ply) then return end
		local ent = ply:GetVehicle()
		if not IsValid(ent) then return end
		if not IsValid(ent:GetParent()) then return end
		local veh = ent:GetParent()
		if not veh.Tram then return end
		if not ply:GetNWBool("DrivingTram", false) then return end

		local speed, power, gear = math.Round(veh:GetVelocity():Length() * 0.09144,0), math.Round(veh:GetNWInt("TramPower", 0), 0), veh:GetNWInt("TramGear", 0)

		ShadowText("Км/ч: "..speed, color_white, {x = 5, y = 35})
		ShadowText("Передача: "..(gear == -1 and "R" or 1), color_white, {x = 5, y = 65})
	end)

	local Sprites = {
		"vcmod/lights/beamdot",
		"vcmod/lights/glow",
		"vcmod/lights/glow_hd"
	}

	local Def_colors = {
		["New"] = Color(177,156,255),
		["Old"] = Color(255,226,148),
		["TurnSignal"] = Color(255,191,0),
		["Red"] = Color(255,63,0),
		["Green"] = Color(0,255,0),
		["Blue"] = Color(0,0,255),
		["White"] = color_white,
	}

net.Receive("TramSyncDat", function(len, ply)
	local dat = net.ReadTable()
	local ent = net.ReadEntity()
	ent.LightTable = dat
	print(ent, dat, table.Count(dat))
end)

hook.Add("PostDrawTranslucentRenderables", "dVehicle_DrawLights", function(isDrawingDepth, isDrawSkybox )
	local ply = LocalPlayer()

	for ind, ent in pairs(ents.FindByClass("rshk_tram_base")) do
		if not IsValid(ent) then continue end
		local stantion = Tram_stations[LocalPlayer():GetNWInt("Transport_NextStation", 1)].name or ""
		surface.SetFont("DermaDefault")
		local textsize = surface.GetTextSize( stantion )
		--print(textsize)

		cam.Start3D2D(ent:LocalToWorld(Vector(0, 310, 123.5)),ent:LocalToWorldAngles(Angle(0,180,70)),.25/(textsize/80))
			draw.SimpleText( stantion, "DermaDefault", 0, 0, Color( 255, 255, 0, 255 ), 1,1 )
		cam.End3D2D()

		local Turn, Run = ent:GetNWBool("Tram_HazardActive", false), ent:GetNWBool("Tram_RunningActive", false)

		if !ent.TurnLightsTime or ent.TurnLightsTime < CurTime() then
			ent.TurnLightActive = ent.TurnLightActive == nil and true or !ent.TurnLightActive

			local snd = ent.TurnLightActive and "vcmod/blnk_in.wav" or "vcmod/blnk_out.wav"

			if Turn then
				ent:EmitSound(snd, 55, 100, 1, CHAN_AUTO)
			end
			ent.TurnLightsTime = CurTime()+.5
		end
        if not ent.LightTable then continue end
		for k, dat in pairs(ent.LightTable) do
			dat.PixVis = dat.PixVis or util.GetPixelVisibleHandle()
			local wpos = ent:LocalToWorld(dat.Pos)
			local visible = util.PixelVisible( wpos, size, dat.PixVis ) or 0
			local col = not isstring(dat.Color) and dat.Color or Def_colors[dat.Color]
			dat.ShouldBeActive = dat.ShouldBeActive == nil and false or dat.ShouldBeActive

			if dat.Type == "Run" then dat.ShouldBeActive = Run 
			elseif dat.Type == "TurnLight" then 
				dat.ShouldBeActive = Turn and ent.TurnLightActive 
			end
			if !dat.ShouldBeActive then continue end


			if not dat.DontInner then
				render.SetMaterial( Material(Sprites[1]) )
				render.DrawSprite(wpos,dat.Size,dat.Size/2,Color(col.r,col.g,col.b,col.a*visible))

				render.SetMaterial( Material(Sprites[2]) )
				render.DrawSprite(wpos,dat.Size*3,dat.Size*3,Color(col.r,col.g,col.b,col.a*visible))
			end

			render.SetMaterial( Material(Sprites[3]) )
			render.DrawSprite(wpos,dat.Size*5,dat.Size*5,Color(col.r,col.g,col.b,col.a*visible))
		end
	end
end)