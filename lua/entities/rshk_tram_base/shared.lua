AddCSLuaFile()

ENT.Type	= "anim"
ENT.PrintName	= "TramBase"
ENT.Category	= "Raskinsk trams"
ENT.Model = "models/dk_cars/rashkinsk/tatra/t3.mdl" 
ENT.Spawnable = true
ENT.AdminOnly = false	
ENT.Tram = true
ENT.MaxSpeed = 60 -- Максималка в км/ч по ДЕФОЛТУ, если она не установлена в списке ниже

local V = {
	Category = "Tatra",
	Name = "Tatra T3",
	Model = "models/dk_cars/rashkinsk/tatra/t3.mdl",
	SpawnMult = 50,

	DoorLock = {
		Vector(55,260,70),
		Angle(180,0,90),
		"models/props_c17/utilitypole01a.mdl"
	},

	Mass = 19000,
	MaxSpeed = 50, -- меняйте максималку на отдельный трамвай здесь
	MaxThrottle = 150000,
	SeatPos = Vector(-10,285.3,65.2),
	SeatAng = Angle(),
	WheelPos = {
		Front = {Vector(0,164.6,-4.8), Angle(), Vector(0,164.6,33.2)},
		Rear = {Vector(0,-137.3,-4.8), Angle(), Vector(0,-137.3,33.2)},
	},

	LightTable = {
		{
			Pos = Vector(22.1, 328.2, 49.9),
			Type = "Run",
			Color = "Old",
			Size = 64,
		},
		{
			Pos = Vector(-22.1, 328.2, 49.9),
			Type = "Run",
			Color = "Old",
			Size = 64,
		},
		{
			Pos = Vector(44, 302.5, 60.3),
			Type = "TurnLight",
			Color = "TurnSignal",
			Size = 64,
		},
		{
			Pos = Vector(-46, 302.5, 60.3),
			Type = "TurnLight",
			Color = "TurnSignal",
			Size = 64,
		},
		-- back
		{
			Pos = Vector(36, -297, 54.2),
			Type = "TurnLight",
			Color = "TurnSignal",
			Size = 64,
		},
		{
			Pos = Vector(-36, -297, 54.2),
			Type = "TurnLight",
			Color = "TurnSignal",
			Size = 64,
		},
		{
			Pos = Vector(-36, -297, 50),
			Type = "Run",
			Color = "Red",
			Size = 64,
		},
		{
			Pos = Vector(36, -297, 50),
			Type = "Run",
			Color = "Red",
			Size = 64,
		}
	},
	
	PassengerSeatsD = {
		{
			Vector(-42.4,216.8,57.5),
			Angle()
		},
		{
			Vector(-42.4,178.8,57.5),
			Angle()
		},
		{
			Vector(-42.4,144.1,57.5),
			Angle()
		},
		{
			Vector(-42.4,109,57.5),
			Angle()
		},
		{
			Vector(-42.4,73,57.5),
			Angle()
		},
		{
			Vector(-42.4,39.7,57.5),
			Angle()
		},
		{
			Vector(-42.4,4.5,57.5),
			Angle()
		},
		{
			Vector(-42.4,-27.1,57.5),
			Angle()
		},
		{
			Vector(-42.4,-64.6,57.5),
			Angle()
		},
		{
			Vector(-42.4,-98.1,57.5),
			Angle()
		},
		{
			Vector(-42.4,-131.4,57.5),
			Angle()
		},
		{
			Vector(-42.4,-165.7,57.5),
			Angle()
		},
		{
			Vector(-22.6, -277.2, 57.5),
			Angle()
		},
		{
			Vector(-22.6, -277.2, 57.5),
			Angle()
		},
		{
			Vector(0, -277.2, 57.5),
			Angle()
		},
		{
			Vector(22.6, -277.2, 57.5),
			Angle()
		},
		{
			Vector(41.3,-152.1,57.5),
			Angle()
		},
		{
			Vector(41.3,-119,57.5),
			Angle()
		},
		{
			Vector(41.3,-85.7,57.5),
			Angle()
		},
		{
			Vector(41.3,38.3,57.5),
			Angle()
		},
		{
			Vector(41.3,71.5,57.5),
			Angle()
		},
		{
			Vector(41.3,106.9,57.5),
			Angle()
		},
		{
			Vector(41.3,140.1,57.5),
			Angle()
		},
		{
			Vector(17.6,-152.1,57.5),
			Angle()
		},
		{
			Vector(17.6,-119,57.5),
			Angle()
		},
		{
			Vector(17.6,-85.7,57.5),
			Angle()
		},
		{
			Vector(17.6,38.3,57.5),
			Angle()
		},
		{
			Vector(17.6,71.5,57.5),
			Angle()
		},
		{
			Vector(17.6,106.9,57.5),
			Angle()
		},
		{
			Vector(17.6,140.1,57.5),
			Angle()
		},
		{
			Vector(41.2,205.9,57.5),
			Angle(0,180)
		},
		{
			Vector(39.1,-178.3,65),
			Angle(0,90)
		},
	},

	Slider = {
		Model = "models/dk_cars/rashkinsk/tatra/wh.mdl",
		ConnectVec = Vector(0,0,37.9),
		ConnectBone = "ax",
		CustomSlider = nil,
		Sliders = {
			{Vector(0,42.9), Angle(0,90)},
			{Vector(0,-36.7), Angle(0,90)},
		}
	}
}

list.Set("rshk_trams", "tatrat3", V)