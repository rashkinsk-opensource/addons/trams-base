AddCSLuaFile()

local Tooltbl = {}

TOOL.Category = "Rashkinks Trams"
TOOL.Name = "Остановки"

TOOL.Information = {
	{name = "info", stage = 0},
	{name = "left", stage = 0},
	{name = "left_1", stage = 1},
	{name = "right"},
	{name = "reload"},
}

Tooltbl.Stantions = file.Exists("rshk_trams/"..game.GetMap().."/stantions.dat","DATA") and util.JSONToTable(file.Read("rshk_trams/"..game.GetMap().."/stantions.dat", "DATA")) or {}

TOOL.ClientConVar[ "pathsound" ] = ""
TOOL.ClientConVar[ "npathsound" ] = ""
TOOL.ClientConVar[ "nt" ] = 0

if CLIENT then
	language.Add("tool.tram_stantions.name", "Остановки трамвая")
	language.Add("tool.tram_stantions.desc", "Этим тулом можно сделать остановки!")
	language.Add("tool.tram_stantions.0", "Сделайте 2 точки для остановки и дайте им звук")
	language.Add("tool.tram_stantions.left", "Сделать первую точку")
	language.Add("tool.tram_stantions.left_1", "Сделать вторую точку")
	language.Add("tool.tram_stantions.right", "Обновить остановку")
	language.Add("tool.tram_stantions.reload", "Удалить остановку")
else util.AddNetworkString("SyncStations") end

function TOOL:LeftClick()
	if CLIENT then return true end
	local snd = GetConVar( "tram_stantions_pathsound" ):GetString()
	local nsnd = GetConVar( "tram_stantions_npathsound" ):GetString()
	local nt = GetConVar( "tram_stantions_nt" ):GetInt()
	local ply = self:GetOwner()
	local trend = ply:EyePos() + ply:EyeAngles():Forward()*200
	local tr = util.TraceLine({
		start = ply:EyePos(),
		endpos = trend,
		filter = {ply, self}
	})

	self.ActiveTbl = self.ActiveTbl or {}

	if self:GetStage() != 1 then
		self:SetStage(1)
		ply:SetNWVector("trams_tool_Vec",tr.HitPos)
		table.insert(self.ActiveTbl, tr.HitPos)
	else 
		table.insert(self.ActiveTbl, tr.HitPos)
		table.insert(self.ActiveTbl, snd)
		table.insert(self.ActiveTbl, Vector(self.ActiveTbl[1].x/2,self.ActiveTbl[1].y/2,self.ActiveTbl[1].z/2)+Vector(self.ActiveTbl[2].x/2,self.ActiveTbl[2].y/2,self.ActiveTbl[2].z/2))
		table.insert(self.ActiveTbl, nsnd)
		table.insert(self.ActiveTbl, nt)

		table.insert(Tooltbl.Stantions, self.ActiveTbl)

		self:SetStage(0)
		self.ActiveTbl = {}
		net.Start("SyncStations")
			net.WriteString(util.TableToJSON(Tooltbl.Stantions))
		net.Send(ply)
	end
end

function TOOL:Deploy()
	net.Start("SyncStations")
		net.WriteString(util.TableToJSON(Tooltbl.Stantions))
	net.Send(self:GetOwner())
end

	local function NearestVector(vec, vectors)
		local pos = vec
		local r = math.huge
		local mindistance, seat = r^2
		for i, v in pairs(vectors) do
			local distance = v[4]:DistToSqr(pos)
			if distance < mindistance then
				mindistance, seat = distance, v
			end
		end
		
		return seat
	end

function TOOL:RightClick(trace)
	local ply = self:GetOwner()
	if not ply:IsSuperAdmin() then return end
	local trend = ply:EyePos() + ply:EyeAngles():Forward()*200
	local tr = util.TraceLine({
		start = ply:EyePos(),
		endpos = trend,
		filter = {ply, self}
	})

	for k, v in pairs(Tooltbl.Stantions) do
		local vec = (NearestVector(tr.HitPos, Tooltbl.Stantions))
		if vec and v[4]:Distance(tr.HitPos) <= 100 then
			ply:ChatPrint("Изменяем звук с: '"..v[3].." на: '"..GetConVar( "tram_stantions_pathsound" ):GetString().."'")
			ply:ChatPrint("Изменяем звук с: '"..v[5].." на: '"..GetConVar( "tram_stantions_npathsound" ):GetString().."'")
			ply:ChatPrint("Изменяем продолжительность '"..v[5].."' с: '"..tostring(v[6]).." на: '"..GetConVar( "tram_stantions_nt" ):GetInt().."'")
			v[3] = GetConVar( "tram_stantions_pathsound" ):GetString()
			v[5] = GetConVar( "tram_stantions_npathsound" ):GetString()
			v[6] = GetConVar( "tram_stantions_nt" ):GetInt()
			break
		end
	end
end

function TOOL:Reload()
	local ply = self:GetOwner()
	if not ply:IsSuperAdmin() then return end
	local trend = ply:EyePos() + ply:EyeAngles():Forward()*200
	local tr = util.TraceLine({
		start = ply:EyePos(),
		endpos = trend,
		filter = {ply, self}
	})

	for k, v in pairs(Tooltbl.Stantions) do
		local vec = (NearestVector(tr.HitPos, Tooltbl.Stantions))
		if vec and v[4]:Distance(tr.HitPos) <= 100 then
			table.RemoveByValue(Tooltbl.Stantions,v)
			break
		end
	end
	net.Start("SyncStations")
		net.WriteString(util.TableToJSON(Tooltbl.Stantions))
	net.Send(ply)
end

concommand.Add("removestantions",function(ply)
	if not ply:IsSuperAdmin() then return end
	Tooltbl.Stantions = {}
	net.Start("SyncStations")
		net.WriteString(util.TableToJSON(Tooltbl.Stantions))
	net.Send(ply)
end)

if SERVER then
concommand.Add("savestantions",function(ply)
	if not ply:IsSuperAdmin() then return end
	file.CreateDir("rshk_trams/"..game.GetMap().."/")
	file.Write("rshk_trams/"..game.GetMap().."/stantions.dat", util.TableToJSON(Tooltbl.Stantions))
	ply:ChatPrint("Успешное сохранение остановок!Кол-во остановок: "..table.Count(Tooltbl.Stantions))
end)
end

function TOOL.BuildCPanel(panel)
	panel:TextEntry( "Путь к звуку текущ. станции", "tram_stantions_pathsound" ):SetTooltip("Путь к звуку текущ. станции")
	panel:TextEntry( "Путь к звуку след. станции", "tram_stantions_npathsound" ):SetTooltip("Путь к звуку след. станции")
	panel:TextEntry( "Продолжительность файла выше", "tram_stantions_nt" ):SetTooltip("Укажите сколько длится файл след. остановки")
	panel:Button( "Удалить все станции", "removestantions" )
	panel:Button( "Сохранить все станции", "savestantions" )
end

if SERVER then return end
function TOOL:DrawHUD()
end

local Stantions

net.Receive("SyncStations",function(_, ply)
	ply = ply || LocalPlayer()
	local tbl = net.ReadString()
	Stantions = util.JSONToTable(tbl)
end)

	local function DrawWorldBox(v1, v2, c)
		render.DrawLine( v1, Vector(v1.x, v1.y, v2.z), c, true )
		render.DrawLine( v1, Vector(v1.x, v2.y, v1.z), c, true )
		render.DrawLine( v1, Vector(v2.x, v1.y, v1.z), c, true )

		render.DrawLine( Vector(v1.x,v1.y,v2.z), Vector(v2.x, v1.y, v2.z), c, true )
		render.DrawLine( Vector(v2.x,v1.y,v2.z), Vector(v2.x, v1.y, v1.z), c, true )
		render.DrawLine( Vector(v2.x,v1.y,v1.z), Vector(v2.x, v2.y, v1.z), c, true )
		render.DrawLine( Vector(v1.x,v1.y,v2.z), Vector(v1.x, v2.y, v2.z), c, true )
		render.DrawLine( Vector(v1.x,v2.y,v2.z), Vector(v1.x, v2.y, v1.z), c, true )
		render.DrawLine( Vector(v1.x,v2.y,v1.z), Vector(v2.x, v2.y, v1.z), c, true )

		render.DrawLine( v2, Vector(v2.x, v2.y, v1.z), c, true )
		render.DrawLine( v2, Vector(v2.x, v1.y, v2.z), c, true )
		render.DrawLine( v2, Vector(v1.x, v2.y, v2.z), c, true )

	end
	hook.Add( "PostDrawOpaqueRenderables", "tram_stantions_drawstantions", function()
		local ply, wep = LocalPlayer(), LocalPlayer():GetActiveWeapon()
		if not IsValid(wep) or wep.Mode != "tram_stantions" then return end

		local trend = ply:EyePos() + ply:EyeAngles():Forward()*200
		local tr = util.TraceLine({
			start = ply:EyePos(),
			endpos = trend,
			filter = {ply, wep}
		})
		if wep:GetStage() == 1 then
			DrawWorldBox(ply:GetNWVector("trams_tool_Vec",Vector()), tr.HitPos, Color( 255, 255, 0, 255))
		else
			render.SetColorMaterial()
			render.DrawSphere( tr.HitPos, 5, 30, 30, Color( 0, 175, 175, 100 ) )
		end
		if not Stantions then return end

			
		for k, v in pairs(Stantions) do
			local vec = NearestVector(tr.HitPos, Stantions)
			DrawWorldBox(v[1], v[2], Color( ((vec and v[4]:Distance(tr.HitPos) <= 100) and 0 or 255), ((vec and v[4]:Distance(tr.HitPos) <= 100) and 255 or 0), 0, 255))
		end
	end)